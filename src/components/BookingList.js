import React from "react";
import styled from "styled-components";
import { red, redLight } from "../globals/colors";
import RoomList from "./RoomList";

class BookingList extends React.Component {
  navigateTo = path => this.props.history.push(path);

  render() {
    return (
      <Container>
        <Box>
          <Header>Select available slot</Header>
          <ListHeader>This action will require sign in</ListHeader>
          <RoomList
            enableLoginView={this.props.enableLoginView}
            {...this.props}
          />
        </Box>
      </Container>
    );
  }
}

export default BookingList;

const Container = styled.div`
  width: 820px;
  height: 100%;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Box = styled.div`
  width: 820px;
  height: 647px;
  position: relative;
  // display: flex;
  // flex-direction: column;
  background: white;
  border-radius: 10px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  overflow: hidden;
`;

const Header = styled.div`
  height: 58px;
  width: 100%;
  position: relative;
  background: ${red};
  font-size: 36px;
  line-height: 36px;
  padding-top: 40px;
  padding-left: 20px;
  color: white;
`;

const ListHeader = styled.div`
  height: 49px;
  line-height: 49px;
  padding-left: 20px;
  color: white;
  width: 100%;
  position: relative;
  background: ${redLight};
`;
