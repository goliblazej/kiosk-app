import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { red } from "../globals/colors";
import { Fab, TextField } from "@material-ui/core";
import { signIn } from "../actions/auth";
import { addBooking } from "../actions/bookings";

class LoginForm extends React.Component {
  state = {
    username: "",
    password: "",
    loginError: ""
  };

  componentDidUpdate = prevProps => {
    if (prevProps.signInLoading && !this.props.signInLoading) {
      if (this.props.signInError) {
        this.setState({ loginError: "Login failed." });
      } else {
        this.props.addBooking(
          this.props.token,
          this.props.selectedRoom,
          this.props.selectedDate,
          this.props.selectedSlot,
          this.props.state
        );
        this.props.disableLoginView();
      }
    }
  };

  handleBooking = () => {
    const { username, password } = this.state;
    if (!username || !password) {
      this.setState({ loginError: "Please provide username and password" });
    } else {
      this.props.signIn(username, password);
    }
  };

  render() {
    const { username, password, loginError } = this.state;
    return (
      <Container>
        <Header>Sign in</Header>
        <Form>
          <TextField
            label="Login"
            margin="normal"
            variant="outlined"
            value={username}
            onChange={e => this.setState({ username: e.target.value })}
          />
          <TextField
            label="Password"
            margin="normal"
            variant="outlined"
            value={password}
            type="password"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <LoginError>{loginError}</LoginError>
          <SignInButton
            variant="extended"
            color="primary"
            margin="normal"
            onClick={this.handleBooking}
          >
            Confirm booking
          </SignInButton>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  signInLoading: state.auth.signInLoading,
  signInError: state.auth.signInError,
  state: state
});

const mapDispatchToProps = dispatch => ({
  signIn: (email, password) => dispatch(signIn(email, password)),
  addBooking: (token, room, day, slot, state) =>
    dispatch(addBooking(token, room, day, slot, state))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);

const Container = styled.div`
  width: 700px;
  height: 450px;
  position: relative;
  display: flex;
  flex-direction: column;
  margin: 100px auto;
  background: white;
  border-radius: 20px;
  overflow: hidden;
`;

const Header = styled.div`
  height: 50px;
  line-height: 50px;
  padding: 20px;
  font-size: 36px;
  background: ${red};
  position: relative;
  color: white;
`;

const Form = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 50px 30px 0px 30px;
`;

const SignInButton = styled(Fab)`
  &&& {
    height: 70px;
    border-radius: 70px;
    font-size: 18px;
    margin-top: 10px;
  }
`;

const LoginError = styled.div`
  position: relative;
  height: 40px;
  line-height: 40px;
  color: ${red};
  font-size: 16;
  text-align: center;
`;
