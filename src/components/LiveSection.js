import React from "react";
import styled from "styled-components";
import ClockCard from "./ClockCard";
import {
  FaHandsHelping,
  FaNetworkWired,
  FaBriefcase,
  FaUsers,
  FaCalendarAlt,
  FaTags
} from "react-icons/fa";

class LiveSection extends React.Component {
  render() {
    return (
      <Container>
        <ClockCard />
        <Bottom>
          <Title
            style={{
              fontFamily: "Comfortaa"
            }}
          >
            Don't miss!
          </Title>
          <CardContainer>
            <Card onClick={() => this.props.navigate("/offers")}>
              <Icon>
                <FaTags size={"50px"} />
              </Icon>
              <CardTitle>{"OFFERS"}</CardTitle>
            </Card>
            <Card onClick={() => this.props.navigate("/roombooking")}>
              <Icon>
                <FaCalendarAlt size={"50px"} />
              </Icon>
              <CardTitle>{"BOOK STUDY ROOM"}</CardTitle>
            </Card>
            <Card onClick={() => this.props.navigate("/parties")}>
              <Icon>
                <FaUsers size={"50px"} />
              </Icon>
              <CardTitle>{"PARTIES"}</CardTitle>
            </Card>
            <Card onClick={() => this.props.navigate("/studentsupport")}>
              <Icon>
                <FaHandsHelping size={"50px"} />
              </Icon>
              <CardTitle>{"STUDENT SUPPORT"}</CardTitle>
            </Card>
            <Card onClick={() => this.props.navigate("/guestlectures")}>
              <Icon>
                <FaBriefcase size={"50px"} />
              </Icon>
              <CardTitle>{"GUEST LECTURES"}</CardTitle>
            </Card>
            <Card onClick={() => this.props.navigate("/workshops")}>
              <Icon>
                <FaNetworkWired size={"50px"} />
              </Icon>
              <CardTitle>{"WORKSHOPS"}</CardTitle>
            </Card>
          </CardContainer>
        </Bottom>
      </Container>
    );
  }
}

export default LiveSection;

const Container = styled.div`
  width: 800px;
  height: 820px;
  position: relative;
  margin-left: 30px;
`;

const Bottom = styled.div`
  width: 100%;
  height: 503px;
  position: relative;
  padding-left: 70px;
`;

const Title = styled.div`
  font-size: 32px;
  position: relative;
  color: rgba(255, 255, 255, 0.8);
  padding: 12px;
  padding-left: 0;
  font-family: Comfortaa;
`;

const CardContainer = styled.div`
  position: relative;
  height: 380px;
  width: 642px;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.75), 0 10px 10px rgba(0, 0, 0, 0.72);
  border-radius: 20px;
  overflow: hidden;
`;

const Card = styled.div`
  width: 210px;
  height: 187px;
  margin: 2px;
  position: relative;
  background: white;
  color: #e00732;
  float: left;
  display: flex;
  flex-direction: column;

  &:hover {
    opacity: 0.9;
  }
`;

const CardTitle = styled.div`
  font-weight: 500;
  letter-spacing: 1.2px;
  font-size: 20px;
  padding: 15px;
  text-align: center;
`;

const Icon = styled.div`
  height: 95px;
  width: 100%;
  position: relative;
  float: left;
  display: flex;
  align-items: flex-end;
  justify-content: center;
`;
