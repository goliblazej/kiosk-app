import React from "react";
import styled from "styled-components";
import { red } from "../globals/colors";
import MapDialog from "./MapDialog";
import { FaMapMarkedAlt } from "react-icons/fa";
class BankProviderRow extends React.Component {
  state = {
    isMapDialogOpen: false
  };

  render() {
    const { name, overdraft, incentive, studentScore } = this.props.bank;
    const location = {
      description: "54-56 George St, Luton LU1 2AB",
      id: 2,
      latitude: 51.879,
      longitude: -0.416,
      name: "Santander Bank"
    };
    const bankName = name.split("/")[0];
    const accountName = name.split("/")[1];
    return (
      <Row>
        <ImageContainer>
          <Image src={`images/bank-${bankName}.png`} alt={bankName} />
        </ImageContainer>
        <AccountName>{accountName}</AccountName>
        <Cell width={200}>up to £{overdraft}</Cell>
        <RichCell width={250}>{incentive}</RichCell>
        <Cell width={230}>{studentScore}%</Cell>
        <Cell
          width={100}
          onClick={() => this.setState({ isMapDialogOpen: true })}
        >
          <FaMapMarkedAlt size="42px" color={red} />
        </Cell>
        <MapDialog
          location={location}
          open={this.state.isMapDialogOpen}
          onClose={() => this.setState({ isMapDialogOpen: false })}
        />
      </Row>
    );
  }
}
export default BankProviderRow;

const Row = styled.div`
  height: 66.8px;
  line-height: 66.8px;
  position: relative;
  border-bottom: 1px solid grey;
  display: flex;
  flex-direction: row;
  padding-left: 25px;
`;

const Cell = styled.div`
  position: relative;
  width: ${props => (props.width ? props.width - 20 : 200)}px;
  height: 66.8px;
  line-height: 66.8px;
  font-size: 15px;
  display: flex;
  align-items: center;
`;

const RichCell = styled.div`
  position: relative;
  height: 66.8px;
  width: ${props => (props.width ? props.width : 200)}px;
  font-size: 15px;
  display: flex;
  line-height: 22px;
  align-items: center;
  padding-right: 20px;
`;

const Image = styled.img`
  vertical-align: middle;
  max-height: 50px;
`;

const ImageContainer = styled.div`
  position: relative;
  height: 66.8px;
  width: 120px;
  margin-right: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const AccountName = styled.div`
  width: 350px;
  position: relative;
  height: 66.8px;
  line-height: 66.8px;
  font-size: 15px;
`;
