import React from "react";
import styled from "styled-components";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import { mapStyles } from "../globals/colors";

class MapDialog extends React.Component {
  render() {
    const { onClose, open } = this.props;
    const { location } = this.props;
    console.log(location);
    if (!location) return <div />;
    return (
      <RoundedDialog onClose={onClose} open={open}>
        <DialogTitle>{location ? location.name : "Map"}</DialogTitle>
        <DialogText>{location ? location.description : "none"}</DialogText>
        <MapContainer>
          <Map
            google={this.props.google}
            zoom={14}
            styles={mapStyles}
            initialCenter={{
              lat: location.latitude,
              lng: location.longitude
            }}
          >
            <Marker
              name={"Current location"}
              position={{ lat: location.latitude, lng: location.longitude }}
              // icon={{
              //   url: "images/marker.png",
              //   anchor: new this.props.google.maps.Point(32, 32),
              //   scaledSize: new this.props.google.maps.Size(64, 64)
              // }}
            />
          </Map>
        </MapContainer>
      </RoundedDialog>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyAQ6LBuawj5RiESkoxSoSU8wT6xTazZS1I"
})(MapDialog);

const MapContainer = styled.div`
  position: relative;
  width: 580px;
  height: 449px;
  overflow: none;
`;

const RoundedDialog = styled(Dialog)`
  .MuiPaper-rounded {
    border-radius: 20px;
  }
`;

const DialogText = styled.div`
  font-size: 16px;
  padding: 16px 24px;
  padding-top: 0;
`;
