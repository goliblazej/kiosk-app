import React from "react";
import styled from "styled-components";
import { red } from "../globals/colors";
import {
  FaPiggyBank,
  FaHeartbeat,
  FaUniversity,
  FaVolleyballBall,
  FaPercent
} from "react-icons/fa";

class InfoSection extends React.Component {
  render() {
    return (
      <Container>
        <Title>Informations</Title>
        <CardContainer>
          <Card wide onClick={() => this.props.navigate("/bankproviders")}>
            <FaPiggyBank size={"80px"} />
            <CardTitle size={24}>STUDENT BANK ACCOUNTS</CardTitle>
          </Card>
          <Card onClick={() => this.props.navigate("/studenthealth")}>
            <FaHeartbeat size={"50px"} />
            <CardTitle>STUDENT HEALTH</CardTitle>
          </Card>
          <Card onClick={() => this.props.navigate("/campuses")}>
            <FaUniversity size={"50px"} />
            <CardTitle>CAMPUSES</CardTitle>
          </Card>
          <Card onClick={() => this.props.navigate("/sport")}>
            <FaVolleyballBall size={"50px"} />
            <CardTitle>SPORT</CardTitle>
          </Card>
          <Card onClick={() => this.props.navigate("/")}>
            <FaPercent size={"50px"} />
            <CardTitle>DISCOUNTS</CardTitle>
          </Card>
        </CardContainer>
      </Container>
    );
  }
}
export default InfoSection;

const Container = styled.div`
  width: 500px;
  height: 820px;
  position: relative;
`;

const Title = styled.div`
  font-size: 32px;
  position: relative;
  color: rgba(255, 255, 255, 0.8);
  padding: 12px;
  font-family: Comfortaa;
`;

const CardContainer = styled.div`
  position: relative;
  height: 750px;
  width: 100%;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.75), 0 10px 10px rgba(0, 0, 0, 0.72);
  border-radius: 20px;
  overflow: hidden;
`;

const Card = styled.div`
  width: calc(${props => (props.wide ? 100 : 50)}% - 4px);
  min-width: calc(${props => (props.wide ? 100 : 50)}% - 4px);
  height: 248px;
  margin: 2px;
  position: relative;
  color: white;
  background: ${red};
  float: left;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  &:hover {
    background: #dc143c;
  }
`;

const CardTitle = styled.div`
  margin-top: 20px
  font-weight: 400;
  letter-spacing: 1.2px;
  font-size: ${props => (props.size ? props.size : 20)}px;
`;
