import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { red, redLight } from "../globals/colors";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { fetchRooms } from "../actions/rooms";
import { Button } from "@material-ui/core";

class RoomListElement extends React.Component {
  handleChange = panel => (event, isExpanded) => {
    this.props.setExpanded(isExpanded ? panel : false);
  };

  handleSlotClick = index => {
    this.props.onSlotChange(this.props.selectedSlot === index ? -1 : index);
  };

  createSlots = () => {
    if (this.props.bookings && this.props.bookings.content) {
      const temp = this.props.bookings.content
        .filter(booking => {
          const monthMatch =
            parseInt(booking.startDate.slice(9).split("-")[1]) ===
            this.props.selectedDate.getMonth() + 1;
          const dayMatch =
            parseInt(booking.startDate.slice(9).split("-")[0]) ===
            this.props.selectedDate.getDate();
          const roomMatch = parseInt(booking.roomId) === this.props.room.id;

          return monthMatch && dayMatch && roomMatch;
        })
        .map(booking => parseInt(booking.startDate.slice(0, 2)));

      let slots = [];
      for (let index = 0; index < 24; index++) {
        if (!temp.find(t => t === index)) {
          slots.push(
            <Slot
              key={index}
              onClick={() => this.handleSlotClick(index)}
              selected={this.props.selectedSlot === index}
            >
              {index}:00 - {index}:59
            </Slot>
          );
        }
      }
      return slots;
    }
    return [];
  };

  render() {
    const {
      room,
      expanded,
      enableLoginView,
      selectedSlot,
      onSlotChange
    } = this.props;
    return (
      <ExpansionPanel
        expanded={expanded === room.name}
        onChange={this.handleChange(room.name)}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>{room.name}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <SlotContainer>
            <SlotSelector>{this.createSlots()}</SlotSelector>
            <SlotButton>
              <Button
                disabled={this.props.selectedSlot === -1}
                variant="contained"
                color="primary"
                onClick={enableLoginView}
              >
                Continue
              </Button>
            </SlotButton>
          </SlotContainer>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

const mapStateToProps = state => ({
  rooms: state.rooms.rooms,
  roomsLoading: state.rooms.roomsLoading,
  roomsError: state.rooms.roomsError
});

const mapDispatchToProps = dispatch => ({
  fetchRooms: () => dispatch(fetchRooms())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomListElement);

const SlotContainer = styled.div`
  height: 165px;
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
`;

const SlotSelector = styled.div`
  height: 120px;
  width: 100%;
  position: relative;
  display: block;
`;

const Slot = styled.div`
  height: 40px;
  width: 90px;
  line-height: 40px;
  position: relative;
  font-size: 14px;
  float: left;
  padding-left: 6px;
  background-color: ${props => (props.selected ? red : "transparent")};
  color: ${props => (props.selected ? "white" : "black")};
  border-radius: 10px;
  font-weight: ${props => (props.selected ? 500 : 400)};
  &:hover {
    background-color: ${props => (props.selected ? red : redLight)};
    color: white;
    font-weight: 500;
  }
`;

const SlotButton = styled.div`
  height: 165px;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
`;
