import React from "react";
import styled from "styled-components";
import { red, redLight, redDark, greyDark } from "../globals/colors";
import InfiniteCalendar from "react-infinite-calendar";
import "react-infinite-calendar/styles.css"; // Make sure to import the default stylesheet

class BookingCalendar extends React.Component {
  render() {
    const today = new Date();
    const maxDate = new Date(
      today.getFullYear(),
      today.getMonth() + 4,
      today.getDate()
    );
    return (
      <Container>
        <Calendar
          width={480}
          height={500}
          selected={this.props.selected}
          minDate={today}
          min={today}
          maxDate={maxDate}
          max={maxDate}
          onSelect={this.props.onChange}
          theme={{
            selectionColor: redLight,
            textColor: {
              default: "#333",
              active: "#FFF"
            },
            weekdayColor: redLight,
            headerColor: red,
            floatingNav: {
              background: redDark,
              color: "#FFF",
              chevron: greyDark
            }
          }}
        />
      </Container>
    );
  }
}

export default BookingCalendar;

const Container = styled.div`
  width: 500px;
  height: 100%;
  position: relative;
  display: flex;
  align-items: center;
  margin-left: 10px;
`;

const Calendar = styled(InfiniteCalendar)`
  &&&& {
    border-radius: 10px;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
    overflow: hidden;
  }
`;
