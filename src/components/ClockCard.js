import React from "react";
import styled from "styled-components";
import Clock from "react-live-clock";

class ClockCard extends React.Component {
  render() {
    return (
      <Container>
        <Clock format={"HH:mm:ss"} ticking={true} timezone={"Europe/London"} />
      </Container>
    );
  }
}

export default ClockCard;

const Container = styled.div`
  height: calc(317px - 30px);
  position: relative;

  color: #e00732;
  font-size: 145px;
  text-align: right;
  padding-top: 30px;
  padding-right: 80px;
`;
