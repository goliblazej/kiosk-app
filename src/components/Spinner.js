import React from "react";
import styled from "styled-components";
import MDSpinner from "react-md-spinner";
import { red } from "../globals/colors";

class Spinner extends React.Component {
  render() {
    return (
      <Container>
        <MDSpinner singleColor={red} size={50} />
      </Container>
    );
  }
}

export default Spinner;

const Container = styled.div`
  width: 100%;
  padding-top: 50px;
  display: flex;
  justify-content: center;
`;
