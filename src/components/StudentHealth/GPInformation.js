import React from "react";
import styled from "styled-components";
import { red } from "../../globals/colors";
import Typography from "@material-ui/core/Typography";
import GPLocations from "./GPLocations";

class GPInformation extends React.Component {
  render() {
    return (
      <Container>
        <Title>General Practitioner (GP) surgeries</Title>
        <ul>
          <li>
            <Typography component="div">
              The University works in partnership with local GP surgeries / GP
              practices to support your health needs.
            </Typography>
          </li>
          <li>
            <Typography component="div">
              You will need to register with a local doctor (GP) when you come
              to University - do not leave this until you are unwell.
            </Typography>
          </li>
          <li>
            <Typography component="div">
              We recommend you register with a GP during your induction week
              when representatives are on campus.
            </Typography>
          </li>
          <li>
            <Typography component="div">
              Registration with any GP surgery is free but you can only register
              with one GP / Health Centre at any time.
            </Typography>
          </li>
        </ul>

        <GPLocations />
      </Container>
    );
  }
}

export default GPInformation;

const Container = styled.div`
  width: calc(50% - 5px);
  position: relative;
  background: white;
  padding: 30px;
  border-radius: 0 20px 20px 0;
`;

const Title = styled.div`
  height: 30px;
  line-height: 30px;
  font-size: 18px;
  font-weight: 700;
  color: ${red};
`;
