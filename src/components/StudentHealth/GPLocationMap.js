import React from "react";
import styled from "styled-components";
import { mapStyles } from "../../globals/colors";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";

class GPLocationMap extends React.Component {
  render() {
    const { location } = this.props;
    return (
      <MapContainer>
        <Map
          google={this.props.google}
          zoom={14}
          styles={mapStyles}
          initialCenter={location}
        >
          <Marker
            name={"Current location"}
            position={location}
            icon={{
              url: "images/hospital-icon.png",
              anchor: new this.props.google.maps.Point(32, 32),
              scaledSize: new this.props.google.maps.Size(64, 64)
            }}
          />
        </Map>
      </MapContainer>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyAQ6LBuawj5RiESkoxSoSU8wT6xTazZS1I"
})(GPLocationMap);

const MapContainer = styled.div`
  position: relative;
  width: 600px;
  height: 310px;
  overflow: none;
  margin-bottom: 10px;
`;
