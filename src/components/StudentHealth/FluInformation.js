import React from "react";
import styled from "styled-components";
import { red } from "../../globals/colors";
import Typography from "@material-ui/core/Typography";

class FluInformation extends React.Component {
  render() {
    return (
      <Container>
        <Title>General advice for students about colds and flu</Title>

        <Typography
          component="div"
          style={{ padding: 7, paddingLeft: 20, paddingRight: 20 }}
        >
          Colds and flu can occur throughout the year, but appear to be more
          common in the winter months. It’s important to be aware of the
          different types of flu-like illnesses you may encounter and look after
          yourself.
        </Typography>

        <Typography
          component="div"
          style={{ padding: 7, paddingLeft: 20, paddingRight: 20 }}
        >
          The main symptoms of a cold are sneezing, a sore throat and a blocked
          or runny nose. This may be accompanied by a high temperature,
          tiredness and headache. The flu is more severe and symptoms develop
          quickly, which usually include a fever, severe aches and pains, and
          exhaustion.
        </Typography>

        <Typography
          component="div"
          style={{ padding: 7, paddingLeft: 20, paddingRight: 20 }}
        >
          The best way to fight off infection is to have a healthy lifestyle,
          eat a healthy diet, take regular exercise, get plenty of rest and
          follow good standards of hygiene.
        </Typography>

        <Subtitle>
          Steps you can take to minimise the risk of catching or spreading colds
          and flu:
        </Subtitle>
        <ul
          style={{
            letterSpacing: "0.00938em"
          }}
        >
          <li>
            Wash your hands frequently – cold and flu viruses can be passed from
            hands and other surfaces
          </li>
          <li>
            Avoid touching your eyes, nose and mouth to avoid the spread of
            infections
          </li>
          <li>
            Use disposable tissues and dispose of them immediately, don’t leave
            used tissues lying around
          </li>
          <li>
            Cover your mouth when you cough or sneeze to avoid spreading a virus
          </li>
          <li>Avoid close contact with anyone who has cold or flu symptoms</li>
        </ul>
        <Subtitle>
          Some general advice if you develop cold or flu-like symptoms:
        </Subtitle>
        <ul
          style={{
            letterSpacing: "0.00938em"
          }}
        >
          <li>Drink plenty of water and warm fluids to avoid dehydration</li>
          <li>Keep warm and rest, avoid strenuous activity</li>
          <li>
            Treat pain, fever or discomfort with over the counter medicines and
            decongestants
          </li>
          <li>
            Minimise contact with other people for the first few days to reduce
            the risk of spreading infection
          </li>
          <li>
            Tell a friend you’re feeling poorly and ask them to check on you in
            case you become more acutely ill
          </li>
        </ul>
      </Container>
    );
  }
}

export default FluInformation;

const Container = styled.div`
  width: calc(50% - 5px);
  position: relative;
  background: white;
  padding: 30px;
  margin-right: 10px;
  border-radius: 20px 0 0 20px;
`;

const Title = styled.div`
  height: 30px;
  line-height: 30px;
  font-size: 18px;
  font-weight: 700;
  color: ${red};
`;

const Subtitle = styled.div`
  width: 100%;
  height: 24px;
  line-height: 24px;
  font-size: 16px;
  font-weight: 500;
  color: ${red};
`;
