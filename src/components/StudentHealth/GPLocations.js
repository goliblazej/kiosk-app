import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import GPLocationMap from "./GPLocationMap";

class GPLocations extends React.Component {
  state = {
    value: 0
  };

  render() {
    const { value } = this.state;
    return (
      <div>
        <AppBar position="static">
          <Tabs
            value={value}
            variant="fullWidth"
            onChange={(e, newValue) => this.setState({ value: newValue })}
          >
            <Tab label="Luton" />
            <Tab label="Bedford" />
            <Tab label="Milton Keynes" />
          </Tabs>
        </AppBar>
        {value === 0 && (
          <TabContainer>
            <GPLocationMap location={{ lat: 51.87626, lng: -0.414215 }} />
            Castle Medical Practice is a loal GP surgery for students at the
            Luton campus. It is a 5 minute walk from the University Square
            building.
          </TabContainer>
        )}
        {value === 1 && (
          <TabContainer>
            <GPLocationMap location={{ lat: 52.143828, lng: -0.455763 }} />
            Goldington Avenue Surgery is the dedicated GP practice for students
            at the Bedford campus.
          </TabContainer>
        )}
        {value === 2 && (
          <TabContainer>
            <GPLocationMap location={{ lat: 52.042154, lng: -0.775029 }} />
            Central Milton Keynes Medical Centre is the nearest GP surgery to
            Milton Keynes campus. It is easy to walk from the campus using the
            'redways' cycle/footpaths. Registration: 10.30am – 3.30pm Monday -
            Friday.
          </TabContainer>
        )}
      </div>
    );
  }
}

export default GPLocations;

function TabContainer(props) {
  return <Typography component="div">{props.children}</Typography>;
}
