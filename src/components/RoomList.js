import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { fetchRooms } from "../actions/rooms";
import Spinner from "./Spinner";
import RoomListElement from "./RoomListElement";

class RoomList extends React.Component {
  render() {
    const {
      rooms,
      roomsLoading,
      roomsError,
      enableLoginView,
      selectedRoom,
      onRoomChange,
      bookingsLoading
    } = this.props;

    return (
      <Container>
        {roomsLoading || bookingsLoading ? (
          <Spinner />
        ) : roomsError ? (
          <div>Error</div>
        ) : rooms && rooms.content.length > 0 ? (
          rooms.content.map((room, index) => (
            <RoomListElement
              enableLoginView={enableLoginView}
              key={index}
              room={room}
              expanded={selectedRoom}
              setExpanded={expanded => onRoomChange(expanded)}
              {...this.props}
            />
          ))
        ) : (
          <div>No rooms</div>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  rooms: state.rooms.rooms,
  roomsLoading: state.rooms.roomsLoading,
  roomsError: state.rooms.roomsError,
  bookings: state.bookings.bookings,
  bookingsLoading: state.bookings.bookingsLoading
});

const mapDispatchToProps = dispatch => ({
  fetchRooms: () => dispatch(fetchRooms())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomList);

const Container = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`;
