import React from "react";
import styled from "styled-components";

class EventCard extends React.Component {
  navigateTo = path => this.props.history.push(path);

  render() {
    const { event } = this.props;
    console.log(event);
    return (
      <Card>
        <CardTitle>{event.name}</CardTitle>
        <StartDate>Date: {event.startDate}</StartDate>
        <Location>Location: {event.location}</Location>
        <Description>{event.description}</Description>
      </Card>
    );
  }
}

export default EventCard;
//  description: "description from server";
//  endDate: "01:00:00 25-06-2019";
//  id: 1;
//  locationId: 1;
//  name: "Event1";
//  startDate: "01:00:00 26-05-2019";
//  type: "CONCERT";
const Card = styled.div`
  width: 290px;
  height: 340px;
  margin: 15px;
  position: relative;
  background: white;
  float: left;
  border-radius: 10px;
  overflow: hidden;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.35), 0 10px 10px rgba(0, 0, 0, 0.32);
  display: flex;
  flex-direction: column;
`;

const CardTitle = styled.div`
  position: relative;
  font-size: 22px;
  padding: 16px;
`;

const StartDate = styled.div`
  position: relative;
  font-size: 16px;
  padding: 5px;
  padding-left: 16px;
  font-weight: 500;
`;

const Location = styled.div`
  position: relative;
  font-size: 16px;
  padding: 5px;
  padding-left: 16px;
  font-weight: 500;
`;

const Description = styled.div`
  position: relative;
  font-size: 16px;
  padding: 16px;
`;
