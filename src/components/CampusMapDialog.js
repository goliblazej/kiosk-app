import React from "react";
import styled from "styled-components";
import DialogTitle from "@material-ui/core/DialogTitle";
import lutonMap from "../images/luton-campus-map.png";
import bedfordMap from "../images/bedford-campus-map.png";

class CampusMapDialog extends React.Component {
  componentWillMount = () => {
    document.addEventListener("mousedown", this.handleClick, false);
  };

  componentWillUnmount = () => {
    document.addEventListener("mousedown", this.handleClick, false);
  };

  handleClick = e => {
    if (e && this.dialog) {
      if (this.dialog.contains(e.target)) {
        console.log("clicked inside");
        return;
      }
      this.props.onClose();
    }
  };

  componentDidUpdate = prevProps => {
    if (!prevProps.open && this.props.open) {
      this.background.setAttribute(
        "style",
        `background-color: rgba(0,0,0,0.5); z-index: 89;`
      );
      this.dialog.setAttribute("style", `opacity: 1; z-index: 90;`);
    } else if (prevProps.open && !this.props.open) {
      this.background.setAttribute(
        "style",
        `background-color: rgba(0,0,0,0.0); z-index: -1;`
      );
      this.dialog.setAttribute("style", `opacity: 0; z-index: -1;`);
    }
  };

  render() {
    return (
      <MyDialogBox ref={background => (this.background = background)}>
        <MyDialog
          ref={dialog => (this.dialog = dialog)}
          width={this.props.campus === "luton" ? 842 : 1000}
          height={this.props.campus === "luton" ? 595 : 707}
        >
          <img
            src={this.props.campus === "luton" ? lutonMap : bedfordMap}
            alt="map"
          />
        </MyDialog>
      </MyDialogBox>
    );
  }
}

export default CampusMapDialog;

const MyDialogBox = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: -1;
  transition: background-color 0.9s ease;
`;

const MyDialog = styled.div`
  margin-top: -150px;
  width: ${props => props.width};
  height: ${props => props.height};
  position: relative;
  background: white;
  border-radius: 20px;
  z-index: -1;
  opacity: 0;
  overflow: hidden;
  transition: opacity 0.9s ease;
`;
