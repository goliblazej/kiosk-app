import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { fetchBankProviders } from "../actions/bankProviders";
import { red } from "../globals/colors";
import Spinner from "./Spinner";
import BankProviderRow from "./BankProviderRow";
class BankProvidersList extends React.Component {
  componentDidMount = () => {
    this.props.fetchBankProviders();
  };

  getList = () => {
    const {
      bankProviders,
      bankProvidersLoading,
      bankProvidersError
    } = this.props;
    if (bankProvidersLoading) return <Spinner />;
    if (bankProvidersError) return <div>Error</div>;
    if (bankProviders && bankProviders.content.length > 0) {
      return bankProviders.content.map((bank, index) => (
        <BankProviderRow bank={bank} id={index} />
      ));
    }
  };

  render() {
    return (
      <List>
        <Header>
          <HeaderCell width={140}>Bank</HeaderCell>
          <HeaderCell width={350}>Account Name</HeaderCell>
          <HeaderCell width={200}>0% Overdraft</HeaderCell>
          <HeaderCell width={250}>Incentive</HeaderCell>
          <HeaderCell width={210}>Student score</HeaderCell>
          <HeaderCell width={100}>Location</HeaderCell>
        </Header>
        {this.getList()}
      </List>
    );
  }
}

const mapStateToProps = state => ({
  bankProviders: state.bankProviders.bankProviders,
  bankProvidersLoading: state.bankProviders.bankProvidersLoading,
  bankProvidersError: state.bankProviders.bankProvidersError
});

const mapDispatchToProps = dispatch => ({
  fetchBankProviders: () => dispatch(fetchBankProviders())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BankProvidersList);

const List = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
  background: white;
  border-radius: 20px;
  overflow: hidden;
`;

const Header = styled.div`
  height: 81px;
  position: relative;
  background: ${red};
  padding-left: 35px;
  display: flex;
  flex-direction: row;
  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2),
    0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);
`;

const HeaderCell = styled.div`
  height: 80px;
  line-height: 80px;
  width: ${props => (props.width ? props.width : 200)}px;
  color: white;
  font-size: 22px;
  font-weight: 500;
`;
