import React from "react";
import styled from "styled-components";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import BankProvidersPage from "../pages/BankProvidersPage";
import MainPage from "../pages/MainPage";
import StudentHealthPage from "../pages/StudentHealthPage";
import CampusesPage from "../pages/CampusesPage";
import LutonCampusPage from "../pages/campuses/LutonCampusPage";
import BedfordCampusPage from "../pages/campuses/BedfordCampusPage";
import MkCampusPage from "../pages/campuses/MkCampusPage";
import SportsPage from "../pages/SportsPage";
import BullsSportsPage from "../pages/sports/BullsSportsPage";
import GetActiveSportsPage from "../pages/sports/GetActiveSportsPage";
import StudentSupportPage from "../pages/StudentSupportPage";
import GuestLecturesPage from "../pages/GuestLecturesPage";
import WorkshopsPage from "../pages/WorkshopsPage";
import PartiesPage from "../pages/PartiesPage";
import RoomBookingPage from "../pages/RoomBookingPage";
import BullsSportPage from "../pages/sports/BullsSportPage";
import OffersPage from "../pages/OffersPage";

export const history = createBrowserHistory();

const AppRouter = props => (
  <Router history={history}>
    <Container>
      <Switch>
        <Route path="/" component={MainPage} exact={true} />
        <Route path="/bankproviders" component={BankProvidersPage} />
        <Route path="/studenthealth" component={StudentHealthPage} />
        <Route path="/campuses" component={CampusesPage} exact={true} />
        <Route path="/campuses/luton" component={LutonCampusPage} />
        <Route path="/campuses/bedford" component={BedfordCampusPage} />
        <Route path="/campuses/mk" component={MkCampusPage} />
        <Route path="/sport" component={SportsPage} exact={true} />
        <Route path="/sport/bulls" component={BullsSportsPage} exact={true} />
        <Route path="/sport/bulls/:id" component={BullsSportPage} />
        <Route path="/sport/getactive" component={GetActiveSportsPage} />
        <Route path="/studentsupport" component={StudentSupportPage} />
        <Route path="/guestlectures" component={GuestLecturesPage} />
        <Route path="/workshops" component={WorkshopsPage} />
        <Route path="/parties" component={PartiesPage} />
        <Route path="/roombooking" component={RoomBookingPage} />
        <Route path="/offers" component={OffersPage} />
      </Switch>
    </Container>
  </Router>
);

export default AppRouter;

const Container = styled.div`
  width: 100%;
  height: 100vh;
  overflow: hidden;
  position: relative;
  background: radial-gradient(
    ellipse at center,
    rgba(50, 50, 50, 1) 0%,
    // rgba(40, 40, 40, 1) 10%,
    // rgba(39, 39, 39, 1) 30%,
    // rgba(37, 37, 37, 1) 40%,
    // rgba(35, 35, 35, 1) 50%,
    // rgba(34, 34, 34, 1) 60%,
    // rgba(30, 30, 30, 1) 70%,
    // rgba(27, 27, 27, 1) 80%,
    // rgba(25, 25, 25, 1) 85%,
    rgba(21, 21, 21, 1) 90%,
    // rgba(21, 21, 21, 1) 95%,
    rgba(19, 19, 19, 1) 100%
  );
  background-color: #262626;
  display: flex;
  align-items: center;
  justify-content: center;
`;
