import {
  FETCH_BANK_PROVIDERS_BEGIN,
  FETCH_BANK_PROVIDERS_SUCCESS,
  FETCH_BANK_PROVIDERS_FAILURE
} from "../actions/bankProviders";

const initialState = {
  bankProviders: null,
  bankProvidersLoading: false,
  bankProvidersError: null
};

export default function bankProvidersReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_BANK_PROVIDERS_BEGIN:
      return {
        ...state,
        bankProvidersLoading: true,
        bankProvidersError: null
      };
    case FETCH_BANK_PROVIDERS_SUCCESS:
      return {
        ...state,
        bankProvidersLoading: false,
        bankProviders: action.payload.bankProviders
      };
    case FETCH_BANK_PROVIDERS_FAILURE:
      return {
        ...state,
        bankProvidersLoading: false,
        bankProvidersError: action.payload.error
      };
    default:
      return state;
  }
}
