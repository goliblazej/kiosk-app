import {
  FETCH_ROOMS_BEGIN,
  FETCH_ROOMS_SUCCESS,
  FETCH_ROOMS_FAILURE
} from "../actions/rooms";

const initialState = {
  rooms: null,
  roomsLoading: false,
  roomsError: null
};

export default function roomsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_ROOMS_BEGIN:
      return {
        ...state,
        roomsLoading: true,
        roomsError: null
      };
    case FETCH_ROOMS_SUCCESS:
      return {
        ...state,
        roomsLoading: false,
        rooms: action.payload.rooms
      };
    case FETCH_ROOMS_FAILURE:
      return {
        ...state,
        roomsLoading: false,
        roomsError: action.payload.error
      };
    default:
      return state;
  }
}
