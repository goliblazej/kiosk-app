import {
  FETCH_EVENTS_BEGIN,
  FETCH_EVENTS_SUCCESS,
  FETCH_EVENTS_FAILURE
} from "../actions/events";

const initialState = {
  events: null,
  eventsLoading: false,
  eventsError: null
};

export default function eventsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_EVENTS_BEGIN:
      return {
        ...state,
        eventsLoading: true,
        eventsError: null
      };
    case FETCH_EVENTS_SUCCESS:
      return {
        ...state,
        eventsLoading: false,
        events: action.payload.events
      };
    case FETCH_EVENTS_FAILURE:
      return {
        ...state,
        eventsLoading: false,
        eventsError: action.payload.error
      };
    default:
      return state;
  }
}
