import {
  SIGN_IN_BEGIN,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE
} from "../actions/auth";

const initialState = {
  token: "",
  signInLoading: false,
  signInError: null
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case SIGN_IN_BEGIN:
      return {
        ...state,
        signInLoading: true,
        signInError: null
      };
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        signInLoading: false,
        token: action.payload.token
      };
    case SIGN_IN_FAILURE:
      return {
        ...state,
        signInLoading: false,
        signInError: action.payload.error
      };
    default:
      return state;
  }
}
