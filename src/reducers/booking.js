import {
  FETCH_BOOKINGS_BEGIN,
  FETCH_BOOKINGS_SUCCESS,
  FETCH_BOOKINGS_FAILURE
} from "../actions/bookings";

const initialState = {
  bookings: null,
  bookingsLoading: false,
  bookingsError: null
};

export default function bookingsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_BOOKINGS_BEGIN:
      return {
        ...state,
        bookingsLoading: true,
        bookingsError: null
      };
    case FETCH_BOOKINGS_SUCCESS:
      return {
        ...state,
        bookingsLoading: false,
        bookings: action.payload.bookings
      };
    case FETCH_BOOKINGS_FAILURE:
      return {
        ...state,
        bookingsLoading: false,
        bookingsError: action.payload.error
      };
    default:
      return state;
  }
}
