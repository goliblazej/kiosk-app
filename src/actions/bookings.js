import axios from "axios";
import { serviceUrl } from "../globals/urls";

export const FETCH_BOOKINGS_BEGIN = "FETCH_BOOKINGS_BEGIN";
export const FETCH_BOOKINGS_SUCCESS = "FETCH_BOOKINGS_SUCCESS";
export const FETCH_BOOKINGS_FAILURE = "FETCH_BOOKINGS_FAILURE";
export const ADD_BOOKING_BEGIN = "ADD_BOOKING_BEGIN";
export const ADD_BOOKING_SUCCESS = "ADD_BOOKING_SUCCESS";
export const ADD_BOOKING_FAILURE = "ADD_BOOKING_FAILURE";

export const fetchBookingsBegin = () => ({
  type: FETCH_BOOKINGS_BEGIN
});

export const fetchBookingsSuccess = bookings => ({
  type: FETCH_BOOKINGS_SUCCESS,
  payload: { bookings }
});

export const fetchBookingsFailure = error => ({
  type: FETCH_BOOKINGS_FAILURE,
  payload: { error }
});

export const fetchBookings = () => {
  const requestConfig = {
    method: "GET",
    url: `${serviceUrl}/kiosk/roombooking`,
    json: true
  };

  return async dispatch => {
    dispatch(fetchBookingsBegin());
    try {
      const response = await axios(requestConfig);
      dispatch(fetchBookingsSuccess(response.data));
    } catch (error) {
      dispatch(fetchBookingsFailure(error.response));
    }
  };
};

export const addBookingsBegin = () => ({
  type: ADD_BOOKING_BEGIN
});

export const addBookingSuccess = () => ({
  type: ADD_BOOKING_SUCCESS
});

export const addBookingFailure = error => ({
  type: ADD_BOOKING_FAILURE,
  payload: { error }
});

export const addBooking = (token, room, day, slot, state) => {
  return async dispatch => {
    let roomId = -1;

    state.rooms.rooms.content.map(r => {
      if (r.name === room) roomId = r.id;
    });

    const requestConfig = {
      method: "POST",
      url: `${serviceUrl}/kiosk/roombooking`,
      headers: {
        authorization:
          "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwiaWF0IjoxNTU5MjEwMDI1LCJleHAiOjE1NTk4MTQ4MjV9.-MGJMKVGsWYYp8GclsINn6xMdAg_1xUMJ0g84AIxIXHg_4lTzyKfzFeGxun6LbCah8ntOaCPLGpLO2s7DhZtRA"
      },
      // data: {
      //   // createDate: "10:10:10 30-05-2019",
      //   // createdByUser: 1,
      //   endDate: "",
      //   roomId: roomId,
      //   startDate: `${slot}:00:00 ${day.getDate()}-${
      //     day.getMonth() >= 9 ? day.getMonth() + 1 : "0" + day.getMonth()
      //   }-2019`,
      //   type: "BOOKING",
      //   // updateDate: "10:10:10 30-05-2019",
      //   // updatedByUser: 1,
      //   userId: 1
      // },

      data: {
        createDate: "10:10:10 30-05-2019",
        createdByUser: 1,
        endDate: `${slot <= 9 ? "0" + slot : slot}:59:59 ${day.getDate()}-${
          day.getMonth() >= 9 ? day.getMonth() + 1 : "0" + (day.getMonth() + 1)
        }-2018`,
        id: 0,
        roomId: roomId,
        startDate: `${slot <= 9 ? "0" + slot : slot}:00:00 ${day.getDate()}-${
          day.getMonth() >= 9 ? day.getMonth() + 1 : "0" + (day.getMonth() + 1)
        }-2018`,
        type: "BOOKING",
        updateDate: "10:10:10 30-05-2019",
        updatedByUser: 1,
        userId: 1
      },
      json: true
    };
    console.log(day.getMonth());
    console.log(requestConfig.data);

    dispatch(addBookingsBegin());
    try {
      await axios(requestConfig);
      dispatch(addBookingSuccess());
      dispatch(fetchBookings());
    } catch (error) {
      dispatch(addBookingFailure(error.response));
    }
  };
};
