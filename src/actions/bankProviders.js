import axios from "axios";
import { serviceUrl } from "../globals/urls";

export const FETCH_BANK_PROVIDERS_BEGIN = "FETCH_BANK_PROVIDERS_BEGIN";
export const FETCH_BANK_PROVIDERS_SUCCESS = "FETCH_BANK_PROVIDERS_SUCCESS";
export const FETCH_BANK_PROVIDERS_FAILURE = "FETCH_BANK_PROVIDERS_FAILURE";

export const fetchBankProvidersBegin = () => ({
  type: FETCH_BANK_PROVIDERS_BEGIN
});

export const fetchBankProvidersSuccess = bankProviders => ({
  type: FETCH_BANK_PROVIDERS_SUCCESS,
  payload: { bankProviders }
});

export const fetchBankProvidersFailure = error => ({
  type: FETCH_BANK_PROVIDERS_FAILURE,
  payload: { error }
});

export const fetchBankProviders = () => {
  const requestConfig = {
    method: "GET",
    url: `${serviceUrl}/kiosk/bankprovider`,
    params: {
      size: 10
    },
    json: true
  };

  return async dispatch => {
    dispatch(fetchBankProvidersBegin());
    try {
      const response = await axios(requestConfig);
      dispatch(fetchBankProvidersSuccess(response.data));
    } catch (error) {
      dispatch(fetchBankProvidersFailure(error.response));
    }
  };
};
