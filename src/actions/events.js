import axios from "axios";
import { serviceUrl } from "../globals/urls";

export const FETCH_EVENTS_BEGIN = "FETCH_EVENTS_BEGIN";
export const FETCH_EVENTS_SUCCESS = "FETCH_EVENTS_SUCCESS";
export const FETCH_EVENTS_FAILURE = "FETCH_EVENTS_FAILURE";

export const fetchEventsBegin = () => ({
  type: FETCH_EVENTS_BEGIN
});

export const fetchEventsSuccess = events => ({
  type: FETCH_EVENTS_SUCCESS,
  payload: { events }
});

export const fetchEventsFailure = error => ({
  type: FETCH_EVENTS_FAILURE,
  payload: { error }
});

export const fetchEvents = () => {
  const requestConfig = {
    method: "GET",
    url: `${serviceUrl}/kiosk/event`,
    json: true,
    params: {
      pageSize: 1000
    }
  };

  return async dispatch => {
    dispatch(fetchEventsBegin());
    try {
      const response = await axios(requestConfig);
      dispatch(fetchEventsSuccess(response.data));
    } catch (error) {
      dispatch(fetchEventsFailure(error.response));
    }
  };
};
