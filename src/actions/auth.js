import axios from "axios";
import { serviceUrl } from "../globals/urls";

export const SIGN_IN_BEGIN = "SIGN_IN_BEGIN";
export const SIGN_IN_SUCCESS = "SIGN_IN_SUCCESS";
export const SIGN_IN_FAILURE = "SIGN_IN_FAILURE";

export const signInBegin = () => ({
  type: SIGN_IN_BEGIN
});

export const signInSuccess = token => ({
  type: SIGN_IN_SUCCESS,
  payload: { token }
});

export const signInFailure = error => ({
  type: SIGN_IN_FAILURE,
  payload: { error }
});

export const signIn = (email, password) => {
  const requestConfig = {
    method: "POST",
    url: `${serviceUrl}/kiosk/user/login`,
    data: {
      email: email,
      password: password
    },
    json: true
  };

  return async dispatch => {
    dispatch(signInBegin());
    try {
      const response = await axios(requestConfig);
      dispatch(signInSuccess(response.data));
    } catch (error) {
      dispatch(signInFailure(error.response));
    }
  };
};
