import axios from "axios";
import { serviceUrl } from "../globals/urls";

export const FETCH_ROOMS_BEGIN = "FETCH_ROOMS_BEGIN";
export const FETCH_ROOMS_SUCCESS = "FETCH_ROOMS_SUCCESS";
export const FETCH_ROOMS_FAILURE = "FETCH_ROOMS_FAILURE";

export const fetchRoomsBegin = () => ({
  type: FETCH_ROOMS_BEGIN
});

export const fetchRoomsSuccess = rooms => ({
  type: FETCH_ROOMS_SUCCESS,
  payload: { rooms }
});

export const fetchRoomsFailure = error => ({
  type: FETCH_ROOMS_FAILURE,
  payload: { error }
});

export const fetchRooms = () => {
  const requestConfig = {
    method: "GET",
    url: `${serviceUrl}/kiosk/room`,
    json: true
  };

  return async dispatch => {
    dispatch(fetchRoomsBegin());
    try {
      const response = await axios(requestConfig);
      dispatch(fetchRoomsSuccess(response.data));
    } catch (error) {
      dispatch(fetchRoomsFailure(error.response));
    }
  };
};
