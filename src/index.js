import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import AppRouter from "./routers/AppRouter";
import * as serviceWorker from "./serviceWorker";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { red } from "./globals/colors";
import "./index.css";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: red
    }
  },
  typography: {
    useNextVariants: true
  }
});

const store = configureStore();
const jsx = (
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <AppRouter />
    </MuiThemeProvider>
  </Provider>
);

ReactDOM.render(jsx, document.getElementById("root"));
serviceWorker.unregister();
