import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import InfoSection from "../components/InfoSection";
import LiveSection from "../components/LiveSection";
import { fetchEvents } from "../actions/events";

class MainPage extends React.Component {
  componentDidMount = () => {
    this.props.fetchEvents();
  };
  render() {
    return (
      <Content>
        <InfoSection navigate={this.props.history.push} />
        <LiveSection navigate={this.props.history.push} />
      </Content>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchEvents: () => dispatch(fetchEvents())
});

export default connect(
  undefined,
  mapDispatchToProps
)(MainPage);

const Content = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
