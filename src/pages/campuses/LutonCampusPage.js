import React from "react";
import styled from "styled-components";
import PageTitle from "../PageTitle";
import { red } from "../../globals/colors";
import Typography from "@material-ui/core/Typography";
import MapDialog from "../../components/MapDialog";
import CampusMapDialog from "../../components/CampusMapDialog";

class LutonCampusPage extends React.Component {
  state = {
    isMapDialogOpen: false,
    isCampusDialogOpen: false
  };

  render() {
    const location = {
      description: "1 Vicarage St, Luton LU1 3HZ",
      id: 2,
      latitude: 51.878024,
      longitude: -0.409617,
      name: "Luton Campus"
    };

    return (
      <Page>
        <PageTitle title="Luton Campus" />
        <Back onClick={() => this.props.history.push("/campuses")}>Back</Back>
        <Container>
          <Content>
            <Title>About</Title>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              Our Luton campus is located right in the heart of the town centre.
              It boasts our brand-new, seven-storey library, Postgraduate and
              CPD Centre, as well as an array of fantastic modern facilities to
              support each of the courses on offer at the Luton campus.
            </Typography>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              There's a Moot Court, Business Pods, sports therapy and science
              laboratories, an industry-standard TV studio and a gym. For our
              art and design students we have the Alexon building - a former
              fashion house based in the centre of Luton's Cultural Quarter,
              home to the town's thriving arts and culture scene.
            </Typography>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              At the heart of our Luton campus is the Campus Centre. As well as
              a large lecture theatre, study rooms and a careers centre, it is
              also the place to build your social life, with Café Neo and a
              modern Students’ Union with Metro Bar and Grill, home to Starbucks
              Coffee and regular live music and DJ nights.
            </Typography>
            <Menu>
              <Card onClick={() => this.setState({ isCampusDialogOpen: true })}>
                Campus Map
              </Card>
              <Card onClick={() => this.setState({ isMapDialogOpen: true })}>
                Location
              </Card>
            </Menu>
            {/* CONTACT !!!! as a new card, display all in dialog */}
          </Content>
          <PhotoContainer />
          <MapDialog
            location={location}
            open={this.state.isMapDialogOpen}
            onClose={() => this.setState({ isMapDialogOpen: false })}
          />
          <CampusMapDialog
            open={this.state.isCampusDialogOpen}
            onClose={() => this.setState({ isCampusDialogOpen: false })}
            campus="luton"
          />
        </Container>
      </Page>
    );
  }
}

export default LutonCampusPage;

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
  font-family: Comfortaa;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: row;
  background: white;
  border-radius: 20px;
`;

const Title = styled.div`
  padding: 30px
  padding-bottom: 12px;
  font-size: 22px;
  font-weight: 700;
  color: ${red};
`;

const Content = styled.div`
  width: 60%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: column;
  border-radius: 20px 0 0 20px;
`;

const PhotoContainer = styled.div`
  width: 40%;
  height: 758px;
  position: relative;
  border-radius: 0 20px 20px 0;
  background: url(../images/campus-Luton.jpg);
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

const Menu = styled.div`
  width: 90%;
  margin: 0 auto;
  height: 350px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`;

const Card = styled.div`
  width: 270px
  height: 270px
  background-color: ${red}
  color: white;
  font-weight: 700;
  font-size: 36px;
  border-radius: 15px;
  font-family: Comfortaa;
  display: flex;
  justify-content: center;
  align-items: center;
`;
