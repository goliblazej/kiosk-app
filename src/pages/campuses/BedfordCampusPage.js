import React from "react";
import styled from "styled-components";
import PageTitle from "../PageTitle";
import { red } from "../../globals/colors";
import Typography from "@material-ui/core/Typography";
import MapDialog from "../../components/MapDialog";
import CampusMapDialog from "../../components/CampusMapDialog";

class BedfordCampusPage extends React.Component {
  state = {
    isMapDialogOpen: false,
    isCampusDialogOpen: false
  };

  render() {
    const location = {
      description: "Polhill Ave, Bedford MK41 9EA",
      id: 2,
      latitude: 52.141394,
      longitude: -0.442194,
      name: "Bedford Campus"
    };
    return (
      <Page>
        <PageTitle title="Bedford Campus" />
        <Back onClick={() => this.props.history.push("/campuses")}>Back</Back>
        <Container>
          <Content>
            <Title>About</Title>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              The Bedford campus has lots of green open spaces yet is close to
              Bedford town centre and local amenities. It boasts our Gateway
              building, a three-storey building that offers high-quality
              teaching spaces, informal learning spaces, lecture theatres and a
              new student services centre.
            </Typography>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              There’s also a Forest School for trainee teachers, a 280-seat
              professional theatre and dance studios, a Physical Education and
              Sport Science Centre, which was used to train athletes in the 2012
              Olympics, and a newly refurbished Students' Union social space.
            </Typography>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              Bedford is a riverside town with a mixture of lively traditional
              pubs and restaurants, shops and first-class sports facilities. The
              Corn Exchange in the town centre shows regular music and comedy
              gigs, plus there are museums, landscaped parks, and a cinema.
            </Typography>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              Bedford is well-placed with its main train station linking London
              and Brighton to the south and Leicester, Nottingham and Sheffield
              to the north.
            </Typography>
            <Menu>
              <Card onClick={() => this.setState({ isCampusDialogOpen: true })}>
                Campus Map
              </Card>
              <Card onClick={() => this.setState({ isMapDialogOpen: true })}>
                Location
              </Card>
            </Menu>
          </Content>
          <PhotoContainer />
          <MapDialog
            location={location}
            open={this.state.isMapDialogOpen}
            onClose={() => this.setState({ isMapDialogOpen: false })}
          />
          <CampusMapDialog
            open={this.state.isCampusDialogOpen}
            onClose={() => this.setState({ isCampusDialogOpen: false })}
            campus="bedford"
          />
        </Container>
      </Page>
    );
  }
}

export default BedfordCampusPage;

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
  font-family: Comfortaa;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: row;
  background: white;
  border-radius: 20px;
`;

const Title = styled.div`
  padding: 30px
  padding-bottom: 12px;
  font-size: 22px;
  font-weight: 700;
  color: ${red};
`;

const Content = styled.div`
  width: 60%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: column;
  border-radius: 20px 0 0 20px;
`;

const PhotoContainer = styled.div`
  width: 40%;
  height: 758px;
  position: relative;
  border-radius: 0 20px 20px 0;
  background: url(../images/campus-Bedford.jpg);
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

const Menu = styled.div`
  width: 90%;
  margin: 0 auto;
  height: 350px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`;

const Card = styled.div`
  width: 270px
  height: 270px
  background-color: ${red}
  color: white;
  font-weight: 700;
  font-size: 36px;
  border-radius: 15px;
  font-family: Comfortaa;
  display: flex;
  justify-content: center;
  align-items: center;
`;
