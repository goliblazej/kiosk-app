import React from "react";
import styled from "styled-components";
import PageTitle from "../PageTitle";
import { red } from "../../globals/colors";
import Typography from "@material-ui/core/Typography";
import MapDialog from "../../components/MapDialog";
import CampusMapDialog from "../../components/CampusMapDialog";

class BedfordCampusPage extends React.Component {
  state = {
    isMapDialogOpen: false,
    isCampusDialogOpen: false
  };

  render() {
    const location = {
      description: "502 Avebury Blvd, Milton Keynes MK9 3HS",
      id: 2,
      latitude: 52.03896,
      longitude: -0.756369,
      name: "Milton Keynes Campus"
    };
    return (
      <Page>
        <PageTitle title="Milton Keynes Campus" />
        <Back onClick={() => this.props.history.push("/campuses")}>Back</Back>
        <Container>
          <Content>
            <Title>About</Title>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              Milton Keynes campus is located in central Milton Keynes close to
              shops, businesses and local amenities.
            </Typography>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              It's easy to get to if you live locally and the campus is very
              well connected, being within 35 minutes of London by fast train.
              Study locally and save money if you're MK-based. For example, our
              foundation degrees offer you part-time and flexible learning study
              modes, so you can continue to work full-time and live at home
              whilst studying for your degree. Our with placement degrees also
              give you the opportunity to get real work experience as part of
              your studies.
            </Typography>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              Our courses are relevant to business, education and health, the
              needs of the Milton Keynes knowledge economy and local and
              national businesses.
            </Typography>
            <Typography
              component="div"
              style={{ padding: 7, paddingLeft: 30, paddingRight: 30 }}
            >
              We offer foundation degrees, Honours degrees, Master's degrees and
              Doctorates, as well as Continuing Professional Development (CPD)
            </Typography>
            <Menu>
              <Card onClick={() => this.setState({ isMapDialogOpen: true })}>
                Location
              </Card>
            </Menu>
          </Content>
          <PhotoContainer />
          <MapDialog
            location={location}
            open={this.state.isMapDialogOpen}
            onClose={() => this.setState({ isMapDialogOpen: false })}
          />
        </Container>
      </Page>
    );
  }
}

export default BedfordCampusPage;

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
  font-family: Comfortaa;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: row;
  background: white;
  border-radius: 20px;
`;

const Title = styled.div`
  padding: 30px
  padding-bottom: 12px;
  font-size: 22px;
  font-weight: 700;
  color: ${red};
`;

const Content = styled.div`
  width: 60%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: column;
  border-radius: 20px 0 0 20px;
`;

const PhotoContainer = styled.div`
  width: 40%;
  height: 758px;
  position: relative;
  border-radius: 0 20px 20px 0;
  background: url(../images/campus-MK.jpg);
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

const Menu = styled.div`
  width: 90%;
  margin: 0 auto;
  height: 350px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`;

const Card = styled.div`
  width: 270px
  height: 270px
  background-color: ${red}
  color: white;
  font-weight: 700;
  font-size: 36px;
  border-radius: 15px;
  font-family: Comfortaa;
  display: flex;
  justify-content: center;
  align-items: center;
`;
