import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import PageTitle from "./PageTitle";
import { red } from "../globals/colors";
import { fetchEvents } from "../actions/events";
import EventCard from "../components/EventCard";

class GuestLecturesPage extends React.Component {
  componentDidMount = () => {
    if (!this.props.events) {
      this.props.fetchEvents();
    }
  };

  navigateTo = path => this.props.history.push(path);

  render() {
    const { events } = this.props;
    return (
      <Page>
        <PageTitle title="Guest Lectures" />
        <Back onClick={() => this.navigateTo("/")}>Back</Back>
        <Container>
          {events && events.content.length > 0 ? (
            events.content.map(
              (event, index) =>
                event.type === "GUEST_LECTURE" && (
                  <EventCard key={index} event={event} />
                )
            )
          ) : (
            <Empty>Nothing to display</Empty>
          )}
        </Container>
      </Page>
    );
  }
}

const mapStateToProps = state => ({
  events: state.events.events,
  eventsLoading: state.events.eventsLoading,
  eventsError: state.events.eventsError
});

const mapDispatchToProps = dispatch => ({
  fetchEvents: () => dispatch(fetchEvents())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GuestLecturesPage);

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: space-between;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
`;

const Container = styled.div`
  width: calc(100% - 48px);
  height: 750px;
  position: relative;
  display: block;
  background: ${red};
  border-radius: 20px;
  padding-left: 14px;
  padding-right: 14px;
  padding-top: 8px;
  margin-left: 10px;
`;

const Empty = styled.div`
  width: 250px;
  position: relative;
  height: 120px;
  line-height: 120px;
  text-align: center;
  color: black;
  font-size: 22px;
  background-color: white;
  border-radius: 20px;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.35), 0 10px 10px rgba(0, 0, 0, 0.32);
  margin: 100px auto;
`;
