import React from "react";
import styled from "styled-components";
import PageTitle from "./PageTitle";
import { red } from "../globals/colors";
import Img from "react-image";
import VisibilitySensor from "react-visibility-sensor";

class SportsPage extends React.Component {
  navigateTo = path => this.props.history.push(path);

  render() {
    return (
      <Page>
        <PageTitle title="Sport" />
        <Back onClick={() => this.navigateTo("/")}>Back</Back>
        <Container>
          <Bulls onClick={() => this.navigateTo("/sport/bulls")}>
            <VisibilitySensor>
              <BullsPhoto src="./images/sport-bulls.jpg" />
            </VisibilitySensor>
            <Name>Bedfordshire Bulls</Name>
          </Bulls>
          <BottomSection>
            <GetActive onClick={() => this.navigateTo("/sport/getactive")}>
              <VisibilitySensor>
                <GetActivePhoto src="./images/sports/swimming.jpg" />
              </VisibilitySensor>
              <Name>Get Active</Name>
            </GetActive>
            <AspireGym onClick={() => this.navigateTo("/sport/aspiregym")}>
              <VisibilitySensor>
                <AspireGymPhoto src="./images/sport-aspyregym.jpg" />
              </VisibilitySensor>
              <Name>{"Aspire gyms & Facilities"}</Name>
            </AspireGym>
          </BottomSection>
        </Container>
      </Page>
    );
  }
}

export default SportsPage;

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: space-between;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
  font-family: Comfortaa;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: column;
`;

const BottomSection = styled.div`
  width: 100%;
  height: 335px;
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Name = styled.div`
  height: 40px;
  line-height: 40px;
  font-size: 24px;
  text-transform: uppercase;
  color: white;
  font-weight: 700;
  padding-left: 20px;
  background: ${red};
`;

const Bulls = styled.div`
  z-index: 99;
  width: 100%;
  height: 400px;
  margin-bottom: 20px;
  position: relative;
  display: flex;
  flex-direction: column;
  border-radius: 20px 20px 0 0;
`;

const GetActive = styled.div`
  z-index: 99;
  width: calc(50% - 10px);
  height: 335px;
  position: relative;
  display: flex;
  flex-direction: column;
  border-radius: 0 0 0 20px;
  overflow: hidden;
`;

const AspireGym = styled.div`
  z-index: 99;
  width: calc(50% - 10px);
  height: 335px;
  position: relative;
  display: flex;
  flex-direction: column;
  border-radius: 0 0 20px 0;
  overflow: hidden;
`;

const BullsPhoto = styled(Img)`
  border-radius: 20px 20px 0 0;
  width: 100%;
  height: 360px;
  position: relative;
  overflow: hidden;
`;

const GetActivePhoto = styled(Img)`
  width: 100%;
  height: 295px;
  position: relative;
`;

const AspireGymPhoto = styled(Img)`
  width: 100%;
  height: 300px;
  position: relative;
`;
