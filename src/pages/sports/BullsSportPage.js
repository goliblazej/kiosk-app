import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import PageTitle from "../PageTitle";
import { red } from "../../globals/colors";
import { fetchEvents } from "../../actions/events";
import { Button } from "@material-ui/core";
import MapDialog from "../../components/MapDialog";

const tempSport = {
  name: "Archery",
  trainingTimes: "Tuesday/20:00-22:00,Friday/14:00-16:00",
  description: `/d/Club information/d/We will be competing in the national BUCS competitions throughout the year, with the Indoor Championships taking place in February.
 The club Coach works with beginner and novice archers. During these sessions, you will learn lots about the fundamental skills within archery and have the 
 chance to try out a range of different bow types./d/Venue/d/All of our sessions take place at Alexander Sports Centre. We train weekly on Tuesday evening,
 There are frequent trains from Luton, and a free University bus from the Bedford train station that stops at Alexander Sports Centre, making it an easy venue
 to get to./d/How to join/d/Come along to our taster sessions during Freshers Week. The Club Subscription Fee will contribute to paying our external coach, as
 well as giving you some free Team Stash so you can completely look and feel a part of our team. If you missed the taster sessions during Freshers Week, don’t 
 worry and contact the club directly to find out how to join. Cost: Sport membership £80.00 + Club Subs £50.00 = £130.00`,
  location: {
    name: "Alexander Sports Centre",
    latitude: "52.142180",
    longitude: "-0.484638",
    description: "MK40 2BQ Sidney Rd Bedford"
  }
};

class BullsSportPage extends React.Component {
  state = {
    isMapDialogOpen: false
  };

  componentDidMount = () => {
    if (!this.props.events) {
      this.props.fetchEvents();
    }
  };

  navigateTo = path => this.props.history.push(path);

  render() {
    console.log(this.props.match.params.id);
    return (
      <Page>
        <PageTitle title={`Sport > Bedfordshire Bulls`} />
        <Back onClick={() => this.navigateTo("/sport/bulls")}>Back</Back>
        <Container>
          <Header>{tempSport.name}</Header>
          <Content>
            <Description>
              {tempSport.description.split("/d/").map((item, index) => {
                if (index === 0) return null;
                if (index % 2 === 1) return <Title>{item}</Title>;
                if (index % 2 === 0) return <Text>{item}</Text>;
                return null;
              })}
            </Description>
            <LocationTime>
              <Location>
                <Title>Location</Title>
                <LocationName>{tempSport.location.name}</LocationName>
                <LocationDescription>
                  {tempSport.location.description}
                </LocationDescription>
                <MapButton>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => this.setState({ isMapDialogOpen: true })}
                  >
                    See map
                  </Button>
                </MapButton>
              </Location>
              <TimeSection>
                <Title>Training times</Title>
                {tempSport.trainingTimes.split(",").map(time => (
                  <TrainingDayTime>
                    <TrainingDay>{time.split("/")[0]}</TrainingDay>
                    <TrainingTime>{time.split("/")[1]}</TrainingTime>
                  </TrainingDayTime>
                ))}
              </TimeSection>
            </LocationTime>
          </Content>
        </Container>
        <MapDialog
          location={tempSport.location}
          open={this.state.isMapDialogOpen}
          onClose={() => this.setState({ isMapDialogOpen: false })}
        />
      </Page>
    );
  }
}

const mapStateToProps = state => ({
  events: state.events.events,
  eventsLoading: state.events.eventsLoading,
  eventsError: state.events.eventsError
});

const mapDispatchToProps = dispatch => ({
  fetchEvents: () => dispatch(fetchEvents())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BullsSportPage);

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: space-between;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
`;

const Container = styled.div`
  width: 100%;
  height: 750px;
  position: relative;
  display: block;
  background: white;
  border-radius: 20px;
  overflow: hidden;
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  height: 30px;
  font-size: 30px;
  padding: 35px;
  padding-left: 25px;
  background-color: ${red};
  color: white;
`;

const Content = styled.div`
  flex: 1;
  position: relative;
  display: flex;
  flex-direction: row;
  padding: 15px;
`;

const Description = styled.div`
  position: relative;
  width: 75%;
  height: 100%;
`;

const LocationTime = styled.div`
  position: relative;
  width: 25%;
  display: flex;
  flex-direction: column;
`;

const Location = styled.div`
  postion: relative;
  margin-bottom: 30px;
`;

const TimeSection = styled.div`
  postion: relative;
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const Title = styled.div`
  height: 22px;
  font-size: 22px;
  padding: 8px;
  color: ${red};
  font-weight: 500;
  letter-spacing: 1.1px;
`;

const Text = styled.div`
  padding: 8px;
  padding-right: 50px;
  text-align: justify;
  letter-spacing: 1px;
`;

const LocationName = styled.div`
  padding: 8px;
  letter-spacing: 1px;
  padding-right: 30px;
`;

const LocationDescription = styled.div`
  padding: 8px;
  letter-spacing: 1px;
  padding-right: 30px;
`;

const MapButton = styled.div`
  position: relative;
  display: flex;
  justify-content: flex-end;
  padding-right: 70px;
  padding-top: 20px;
`;

const TrainingDayTime = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  position: relative;
  padding: 8px;
  padding-top: 14px;
  width: 260px;
`;

const TrainingDay = styled.div`
  font-weight: 500;
`;

const TrainingTime = styled.div``;
