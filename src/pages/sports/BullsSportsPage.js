import React from "react";
import styled from "styled-components";
import PageTitle from "../PageTitle";
import { red } from "../../globals/colors";

class BullsSportsPage extends React.Component {
  navigateTo = path => this.props.history.push(path);

  render() {
    return (
      <Page>
        <PageTitle title="Sport > Bedfordshire Bulls" />
        <Back onClick={() => this.navigateTo("/sport")}>Back</Back>
        <Container>
          {sports.map((sport, index) => (
            <Card onClick={() => this.navigateTo("/sport/bulls/" + index)}>
              <Name>{sport}</Name>
            </Card>
          ))}
        </Container>
      </Page>
    );
  }
}

export default BullsSportsPage;

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: space-between;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
`;

const Card = styled.div`
  padding: 20px;
  width: 205px;
  height: 80px;
  margin: 10px;
  position: relative;
  float: left;
  display: flex;
  flex-direction: column;
  background: ${red};
  border-radius: 5px;
`;

const Name = styled.div`
  font-size: 24px;
  color: white;
  font-weight: 500;
`;

const sports = [
  "Archery",
  "Athletics",
  "Badminton",
  "Basketball (men's)",
  "Basketball (women's)",
  "Cheerleading",
  "Combat Sport",
  "Dance Club",
  "Football (men's)",
  "Football (women's)",
  "Futsal (men's)",
  "Futsal (women's)",
  "Hockey (men's)",
  "Hockey (women's)",
  "Korfball",
  "Netball",
  "Rugby (men's)",
  "Rugby (women's)",
  "Swimming",
  "Tennis",
  "Trampolining",
  "Volleyball (men's)",
  "Volleyball (women's)"
];
