import React from "react";
import styled from "styled-components";
import PageTitle from "../PageTitle";
import { red } from "../../globals/colors";

class GetActiveSportsPage extends React.Component {
  navigateTo = path => this.props.history.push(path);

  render() {
    return (
      <Page>
        <PageTitle title="Sport > Get Active" />
        <Back onClick={() => this.navigateTo("/sport")}>Back</Back>
        <Container>
          {sports.map(sport => (
            <Card onClick={() => this.navigateTo("/sport")}>
              <Name>{sport}</Name>
            </Card>
          ))}
        </Container>
      </Page>
    );
  }
}

export default GetActiveSportsPage;

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
`;

const Card = styled.div`
  padding: 20px;
  width: 205px;
  height: 80px;
  margin: 10px;
  position: relative;
  float: left;
  display: flex;
  flex-direction: column;
  background: ${red};
  border-radius: 5px;
`;

const Name = styled.div`
  font-size: 24px;
  color: white;
  font-weight: 500;
`;

const sports = [
  "Jogging",
  "CrossFit",
  "Football / Futsal",
  "Golf",
  "Parkrun",
  "Gym Sessions",
  "Fitness Classes",
  "Swimming",
  "Table Tennis",
  "Futnet & Touchtennis",
  "TaiChi",
  "Walking",
  "Wheelchair Basketball",
  "Yoga & Pilates",
  "Zumba",
  "Boxercise",
  "Badminton"
];
