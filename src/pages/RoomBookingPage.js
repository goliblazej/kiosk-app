import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import PageTitle from "./PageTitle";
import { red } from "../globals/colors";
import BookingCalendar from "../components/BookingCalendar";
import BookingList from "../components/BookingList";
import { fetchRooms } from "../actions/rooms";
import { fetchBookings } from "../actions/bookings";
import LoginForm from "../components/LoginForm";

class RoomBookingPage extends React.Component {
  state = {
    isLoginView: false,
    selectedDate: new Date(),
    selectedRoom: false,
    selectedSlot: -1
  };
  componentDidMount = () => {
    this.props.fetchRooms();
    this.props.fetchBookings();
  };

  navigateTo = path => this.props.history.push(path);

  render() {
    return (
      <Page>
        <PageTitle title="Room Booking" />
        <Back onClick={() => this.navigateTo("/")}>Back</Back>
        <Container>
          {this.state.isLoginView ? (
            <LoginForm
              disableLoginView={() => this.setState({ isLoginView: false })}
              selectedRoom={this.state.selectedRoom}
              selectedDate={this.state.selectedDate}
              selectedSlot={this.state.selectedSlot}
            />
          ) : (
            <>
              <BookingCalendar
                selected={this.state.selectedDate}
                onChange={date => this.setState({ selectedDate: date })}
              />
              <BookingList
                selectedRoom={this.state.selectedRoom}
                selectedDate={this.state.selectedDate}
                selectedSlot={this.state.selectedSlot}
                onRoomChange={room => this.setState({ selectedRoom: room })}
                onSlotChange={slot => this.setState({ selectedSlot: slot })}
                enableLoginView={() => this.setState({ isLoginView: true })}
              />
            </>
          )}
        </Container>
      </Page>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchRooms: () => dispatch(fetchRooms()),
  fetchBookings: () => dispatch(fetchBookings())
});

export default connect(
  undefined,
  mapDispatchToProps
)(RoomBookingPage);

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: space-between;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: row;
  // background: white; ??
  border-radius: 20px;
`;
