import React from "react";
import styled from "styled-components";
import PageTitle from "./PageTitle";
import { red } from "../globals/colors";
import BankProvidersList from "../components/BankProvidersList";

class BankProvidersPage extends React.Component {
  render() {
    return (
      <Content>
        <PageTitle title="Bank Account Providers" />
        <Back onClick={() => this.props.history.push("/")}>Back</Back>
        <BankProvidersList />
      </Content>
    );
  }
}

export default BankProvidersPage;

const Content = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: space-between;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
  font-family: Comfortaa;
`;
