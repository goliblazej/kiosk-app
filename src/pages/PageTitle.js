import React from "react";
import styled from "styled-components";

class PageTitle extends React.Component {
  render() {
    return <Title>{this.props.title ? this.props.title : "Set title"}</Title>;
  }
}

export default PageTitle;

const Title = styled.div`
  font-size: 32px;
  position: relative;
  color: rgba(255, 255, 255, 0.8);
  padding: 12px;
  width: 100%;
  font-family: Comfortaa;
`;
