import React from "react";
import styled from "styled-components";
import PageTitle from "./PageTitle";
import { red } from "../globals/colors";
import FluInformation from "../components/StudentHealth/FluInformation";
import GPInformation from "../components/StudentHealth/GPInformation";

class StudentHealthPage extends React.Component {
  render() {
    return (
      <Page>
        <PageTitle title="Student Health" />
        <Back onClick={() => this.props.history.push("/")}>Back</Back>
        <Container>
          <FluInformation />
          <GPInformation />
        </Container>
      </Page>
    );
  }
}

export default StudentHealthPage;

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: space-between;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
  font-family: Comfortaa;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: row;
`;
