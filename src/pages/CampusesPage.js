import React from "react";
import styled from "styled-components";
import PageTitle from "./PageTitle";
import { red } from "../globals/colors";
import VisibilitySensor from "react-visibility-sensor";

class CampusesPage extends React.Component {
  navigateTo = path => this.props.history.push(path);
  constructor(props) {
    super(props);
    this.lutonPhoto = null;
  }

  getImage = (src, ref) => {
    const image = new Image();
    image.src = src;
    image.onload = () => {
      ref.setAttribute("style", `background-image: url('${src}'); opacity: 1;`);
    };
  };

  componentDidMount() {
    const srcLuton = "./images/campus-Luton.jpg";
    const srcBedford = "./images/campus-Bedford.jpg";
    const srcMK = "./images/campus-MK.jpg";
    this.getImage(srcLuton, this.lutonPhoto);
    this.getImage(srcBedford, this.bedfordPhoto);
    this.getImage(srcMK, this.mkPhoto);
  }

  render() {
    return (
      <Page>
        <PageTitle title="Our Campuses" />
        <Back onClick={() => this.navigateTo("/")}>Back</Back>
        <Container>
          <LutonCampus onClick={() => this.navigateTo("/campuses/luton")}>
            <VisibilitySensor>
              <LutonPhoto ref={lutonPhoto => (this.lutonPhoto = lutonPhoto)} />
            </VisibilitySensor>
            {/* <LutonPhoto /> */}
            <Name>Luton</Name>
          </LutonCampus>
          <BottomSection>
            <BedfordCampus onClick={() => this.navigateTo("/campuses/bedford")}>
              <BedfordPhoto
                ref={bedfordPhoto => (this.bedfordPhoto = bedfordPhoto)}
              />
              <Name>Bedford</Name>
            </BedfordCampus>
            <MkCampus onClick={() => this.navigateTo("/campuses/mk")}>
              <MkPhoto ref={mkPhoto => (this.mkPhoto = mkPhoto)} />
              <Name>Milton Keynes</Name>
            </MkCampus>
          </BottomSection>
        </Container>
      </Page>
    );
  }
}

export default CampusesPage;

const Page = styled.div`
  position: relative;
  width: 1330px;
  height: 820px;
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: space-between;
`;

const Back = styled.div`
  font-size: 32px;
  top: 0;
  position: absolute;
  right: 0;
  color: ${red};
  padding: 12px;
`;

const Container = styled.div`
  width: 100%;
  height: 758px;
  position: relative;
  display: flex;
  flex-direction: column;
`;

const BottomSection = styled.div`
  width: 100%;
  height: 335px;
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Name = styled.div`
  height: 40px;
  line-height: 40px;
  font-size: 24px;
  text-transform: uppercase;
  color: white;
  font-weight: 700;
  padding-left: 20px;
`;

const LutonCampus = styled.div`
  width: 100%;
  height: 400px;
  margin-bottom: 20px;
  position: relative;
  display: flex;
  flex-direction: column;
  background: ${red};
  border-radius: 20px 20px 0 0;
`;

const BedfordCampus = styled.div`
  width: calc(50% - 10px);
  height: 335px;
  position: relative;
  display: flex;
  flex-direction: column;
  background: ${red};
  border-radius: 0 0 0 20px;
`;

const MkCampus = styled.div`
  width: calc(50% - 10px);
  height: 335px;
  position: relative;
  display: flex;
  flex-direction: column;
  background: ${red};
  border-radius: 0 0 20px 0;
`;

const LutonPhoto = styled.div`
  border-radius: 20px 20px 0 0;
  width: 100%;
  height: 360px;
  position: relative;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  opacity: 0;
  transition: opacity 0.9s ease;
`;

const MkPhoto = styled.div`
  width: 100%;
  height: 295px;
  position: relative;
  // background: url(images/campus-MK.jpg);
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  opacity: 0;
  transition: opacity 0.9s ease;
`;

const BedfordPhoto = styled.div`
  width: 100%;
  height: 300px;
  position: relative;
  // background: url(images/campus-Bedford.jpg);
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  opacity: 0;
  transition: opacity 0.9s ease;
`;
