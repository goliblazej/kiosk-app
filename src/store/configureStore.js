import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import bankProvidersReducer from "../reducers/bankProviders";
import roomsReducer from "../reducers/rooms";
import bookingsReducer from "../reducers/booking";
import eventsReducer from "../reducers/events";
import authReducer from "../reducers/auth";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const store = createStore(
    combineReducers({
      bankProviders: bankProvidersReducer,
      rooms: roomsReducer,
      bookings: bookingsReducer,
      events: eventsReducer,
      auth: authReducer
    }),
    composeEnhancers(applyMiddleware(thunk))
  );

  return store;
};
